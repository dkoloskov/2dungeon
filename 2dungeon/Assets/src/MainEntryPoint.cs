﻿using enums;
using graph;
using graph.enums;
using model;
using System.Collections;
using System.Collections.Generic;
using unity_utils.debug;
using UnityEngine;

public class MainEntryPoint : MonoBehaviour
{
    // Use this for initialization
    void Start ()
    {
        //DebugUtil.Log("System.DateTime.Now.Millisecond: " + System.DateTime.Now.Millisecond);
        UnityEngine.Random.InitState(System.DateTime.Now.Millisecond);

        setDebugConfig();
        setGameSize();
        DebugUtil.Log("Screen.currentResolution: " + Screen.currentResolution);
        DebugUtil.Log("Config.BOTTOM_LEFT: " + Config.BOTTOM_LEFT);

        Graph.Initialize();
        Graph.AddNode(new GameRoot());
        Graph.FireMessage(GameModel.ROOT_NAME, MessageDirectionsEnum.DOWN, GameMessageEnum.INITIALIZE);
    }

    // Update is called once per frame
    void Update ()
    {
        UnityGlobal.Update();
    }

    //--------------------------------------------------------------------------
    //					  PRIVATE\PROTECTED METHODS
    //--------------------------------------------------------------------------
    // Set correct ortographical size, according to resolution
    private void setGameSize()
    {
        float widthDiff = (float) Screen.currentResolution.width / Config.GAME_WIDTH;
        float heightDiff = (float) Screen.currentResolution.height / Config.GAME_HEIGHT;
        if(widthDiff != heightDiff)
        {
            DebugUtil.Log("Non standart resolution | widthDiff:" + widthDiff + " | heightDiff:" + heightDiff);

            float smallerDiff = 1;
            if (widthDiff >= 1 && heightDiff >= 1)
                smallerDiff = (widthDiff < heightDiff) ? widthDiff : heightDiff;
            else if(widthDiff <= 1 && heightDiff <= 1)
                smallerDiff = (widthDiff > heightDiff) ? widthDiff : heightDiff;

            Camera.main.orthographicSize *= smallerDiff;
        }
    }

    private void setDebugConfig()
    {
        //AsyncDebug.Log("Application.persistentDataPath: " + Application.persistentDataPath);
        DebugUtil.LOG_TO_CONSOLE = false;
        //...
    }
}
