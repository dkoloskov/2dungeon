﻿using common_lib.graph.fsm;
using game_graph.fsm.states.choose_dungeon;
using game_graph.fsm.states.dungeon;
using graph.enums;
using model;
using System;
using System.Collections.Generic;
using UnityEngine;


namespace game_graph.fsm
{
    class GameFSM : FSMBase
    {
        public GameFSM(String path)
            :base(path, PassTypesEnum.UP)
        {

        }

        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        protected override void setupChildren()
        {
            AddNode(new DungeonState(_path));
            AddNode(new ChooseDungeonState(_path));
        }

        //--------------------------------------------------------------------------
        //								HANDLERS
        //--------------------------------------------------------------------------
        public override void OnBlock<T>(MessageDirectionsEnum direction, string type, T data = default(T))
        {
            //Debug.Log("STATE: " + stateModel.State);
            base.OnBlock<T>(direction, type, data);
        }

        //--------------------------------------------------------------------------
        //							GETTERS/SETTERS (PROPERTIES)
        //--------------------------------------------------------------------------
        public override string Name
        {
            get { return "GameFSM"; }
        }

        protected override IStateModel stateModel
        {
            get { return GameModel.Instance; }
        }
    }
}
