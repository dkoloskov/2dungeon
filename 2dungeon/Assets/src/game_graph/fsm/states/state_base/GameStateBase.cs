﻿using common_lib.graph.fsm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using graph.enums;
using model;
using enums;
using UnityEngine;
using unity_utils.debug;

namespace game_graph.fsm.states.state_base
{
    public abstract class GameStateBase : FSMStateBase
    {
        public GameStateBase(string path, PassTypesEnum passType = PassTypesEnum.ANY) 
            :base(path, passType)
        {
        }

        protected override void activate()
        {
            base.activate();
            DebugUtil.Log(Name + " state activated");

            FireMessage(GameMessageEnum.STATE_ACTIVATED + Config.STATE_MESSAGE_SEPARATOR + model.State);
        }

        protected override void deactivate()
        {
            base.deactivate();
            DebugUtil.Log(Name + " state DEACTIVATED");
        }

        //--------------------------------------------------------------------------
        //							GETTERS/SETTERS (PROPERTIES)
        //--------------------------------------------------------------------------
        protected GameModel model
        {
            get { return GameModel.Instance; }
        }

        protected override string CHANGE_STATE_MESSAGE
        {
            get { return GameMessageEnum.CHANGE_STATE; }
        }

        // This only for FSM.
        protected override IStateModel stateModel
        {
            get { return model; }
        }
    }
}
