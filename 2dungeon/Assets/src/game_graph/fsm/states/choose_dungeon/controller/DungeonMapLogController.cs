﻿using enums;
using enums.dungeon;
using graph.node;
using model;
using model.dungeon;
using unity_utils.debug;

namespace game_graph.fsm.states.choose_dungeon.controller
{
    class DungeonMapLogController : Node
    {
        public DungeonMapLogController(string path)
            : base(path)
        {
        }

        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        protected override void setupHandlers()
        {
            base.setupHandlers();
            AddHandler(GameMessageEnum.DUNGEON_MAP_LOG, mapLogHandler);
        }

        private void showMapInLog()
        {
            string idMapStr = "=============== GENERATED ID MAP ===============\n";
            string typeMapStr = "=============== GENERATED TYPE MAP ===============\n";
            RoomData roomData;
            for (int y = model.Rooms.Height - 1; y >= 0; y--)
            {
                idMapStr += "\n";
                typeMapStr += "\n";
                for (int x = 0; x < model.Rooms.Width; x++)
                {
                    roomData = model.Rooms[x, y];
                    if (roomData == null)
                    {
                        idMapStr += "   ";
                        typeMapStr += "  ";
                    }
                    else
                    {
                        idMapStr += roomData.ID + " ";
                        if (roomData.ID < 10)
                            idMapStr += " ";

                        typeMapStr += (int) roomData.Type + " ";
                    }
                }
                idMapStr += "\n";
                typeMapStr += "\n";
            }
            idMapStr = typeMapStr + "\n" + idMapStr;

            DebugUtil.Log(idMapStr);
        }

        /*private void showRoomsConnectionsInLog()
        {
            string connectionsSrt = "=============== ROOMS CONNECTIONS / ADJACENCY TABLE ===============\n";

            for (int i = 0; i < model.Dungeon.RoomsAdjacencyTable.Count; i++)
            {
                connectionsSrt += i + " =>";

                for (int j = 0; j < model.Dungeon.RoomsAdjacencyTable[i].Count; j++)
                {
                    if (model.Dungeon.RoomsAdjacencyTable[i][j] != RoomNeighborEnum.NO_NEIGHBOR)
                        connectionsSrt += " " + model.Dungeon.RoomsAdjacencyTable[i][j];
                }
                connectionsSrt += "\n";
            }
            DebugUtil.Log(connectionsSrt);
        }*/

        //--------------------------------------------------------------------------
        //								HANDLERS
        //--------------------------------------------------------------------------
        private void mapLogHandler()
        {
            showMapInLog();
            //showRoomsConnectionsInLog();
        }

        //--------------------------------------------------------------------------
        //							GETTERS/SETTERS (PROPERTIES)
        //--------------------------------------------------------------------------
        public DungeonData model
        {
            get { return GameModel.Instance.Dungeon; }
        }

        public override string Name
        {
            get { return "DungeonMapLogController"; }
        }
    }
}