﻿using System;
using System.Collections.Generic;
using System.Linq;
using common_lib.array;
using common_lib.geom;
using common_lib.more_linq;
using enums;
using enums.dungeon;
using graph.node;
using model;
using model.dungeon;
using model.dungeon.building;
using unity_utils.debug;
using UnityEngine;
using Random = UnityEngine.Random;

namespace game_graph.fsm.states.choose_dungeon.controller.generate_map
{
    public class DungeonMapGenerateController : Node
    {
        private DungeonBuildingSettingsFactory buildingSettingsFactory;
        private DungeonBuildingSettings settings;

        private int roomIDCounter = 0;

        private Point nextKeyRoomPoint;
        private int nextKeyRoomDirection;
        private int nextKeyRoomDirectionMaxShift;
        private Dictionary<int, Point> nextKeyRoomPointConnections;

        private List<Point> availableKeyRoomPoints
            ; // coordinates of key points, which have available directions (without neighbors)


        public DungeonMapGenerateController(string path)
            : base(path)
        {
            buildingSettingsFactory = new DungeonBuildingSettingsFactory();
        }

        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        protected override void setupHandlers()
        {
            base.setupHandlers();
            AddHandler(GameMessageEnum.GENERATE_DUNGEON_MAP, generateDungeonMapHandler);
        }

        private void addKeyRoom()
        {
            RoomData keyRoom = new RoomData(RoomTypeEnum.KEY_POINT, roomIDCounter, nextKeyRoomPoint);
            model.Rooms[nextKeyRoomPoint.X, nextKeyRoomPoint.Y] = keyRoom;

            availableKeyRoomPoints.Add(nextKeyRoomPoint);
            updateAvailableKeyRooms();

            roomIDCounter++;
        }

        private void addCorridorRoom(Point roomPoint)
        {
            model.Rooms[roomPoint.X, roomPoint.Y] = new RoomData(RoomTypeEnum.CORRIDOR, roomIDCounter, roomPoint);
            roomIDCounter++;
        }

        private List<Point> getRoomNeighbors(Point roomPos)
        {
            List<Point> neighbors = new List<Point>();

            Point neighborPoint;
            for (int i = 0; i < RoomNeighborEnum.ALL_DIRECTIONS.Count; i++)
            {
                neighborPoint = getRoomPointByDirectionAndShift(roomPos, i, 1);
                if (!positionInDungeonBounds(neighborPoint))
                    continue;

                if (model.Rooms[neighborPoint.X, neighborPoint.Y] != null)
                    neighbors.Add(neighborPoint);
            }
            return neighbors;
        }

        private void findNextKeyRoomPos()
        {
            // Before search, source KeyRoom set is required.
            Point sourceKeyRoomPoint = availableKeyRoomPoints[Random.Range(0, availableKeyRoomPoints.Count)];
            nextKeyRoomDirection = getNextKeyRoomDirection(sourceKeyRoomPoint);
            nextKeyRoomDirectionMaxShift = getMaxShiftForDirection(sourceKeyRoomPoint, nextKeyRoomDirection);

            nextKeyRoomPoint = null;
            nextKeyRoomPointConnections = new Dictionary<int, Point>();
            findNextKeyRoomPointWithConnections(sourceKeyRoomPoint);

            // IF NO CONNECTIONS
            if (nextKeyRoomPoint == null)
            {
                int shift = Random.Range(settings.CorridorRoomsSequenceMinNum + 1,
                    nextKeyRoomDirectionMaxShift + 1);
                nextKeyRoomPoint = getRoomPointByDirectionAndShift(sourceKeyRoomPoint, nextKeyRoomDirection, shift);
            }
            // Connection of nextKeyRoom with lastKeyRoom, should always be established first
            nextKeyRoomPointConnections[RoomNeighborEnum.GetOpositeDirection(nextKeyRoomDirection)] =
                sourceKeyRoomPoint;
        }

        private void findNextKeyRoomPointWithConnections(Point sourceKeyRoomPoint)
        {
            // Example of related directions.
            // If direction is RIGHT, this means that next key room can connect rooms from TOP, RIGHT, BOTTOM (+LEFT)
            // 4 - 3 - 2
            // |       |
            // 5 - N   1
            // |
            // 6 - 7
            List<int> relatedDirections = RoomNeighborEnum.GetRelatedDirections(nextKeyRoomDirection);

            // Possible connections, for each next possible key room position. Example below
            //            ???
            //            ???
            //4 - 3 - 2   ???
            //|   |   |   |||
            //5 - 8   1 - NNN -???
            //|   |       |||
            //6 - 7       ???
            //            ???
            //            ???
            // Where N - possible key room position, ? - possible connection.
            // Possible connection - when distance between two key rooms is ok to add corridor rooms between them.
            Dictionary<Point, Dictionary<int, Point>> possibleConnections =
                new Dictionary<Point, Dictionary<int, Point>>();
            Dictionary<int, Point> keyRoomPointConnections = null;

            // + 1 - to look not on corridor, but on key room positions
            for (int i = settings.CorridorRoomsSequenceMinNum + 1; i <= nextKeyRoomDirectionMaxShift; i++)
            {
                Point newKeyRoomPoint = getRoomPointByDirectionAndShift(sourceKeyRoomPoint, nextKeyRoomDirection, i);
                keyRoomPointConnections = new Dictionary<int, Point>();
                for (int d = 0; d < relatedDirections.Count; d++)
                {
                    // Key Room
                    Point connection = getConnectionForDirection(newKeyRoomPoint, relatedDirections[d]);
                    if (connection != null)
                    {
                        keyRoomPointConnections[relatedDirections[d]] = connection;
                    }
                }

                if (keyRoomPointConnections.Count > 0)
                    possibleConnections.Add(newKeyRoomPoint, keyRoomPointConnections);
            }

            if (possibleConnections.Count > 0)
            {
                List<Point> possibleConnectionsKeys = possibleConnections.Keys.ToList();
                int randomKeysIndex = Random.Range(0, possibleConnectionsKeys.Count);

                // RESULT OF METHOD IS HERE :)
                nextKeyRoomPoint = possibleConnectionsKeys[randomKeysIndex];
                nextKeyRoomPointConnections = possibleConnections[nextKeyRoomPoint];
            }
        }

        private int getNextKeyRoomDirection(Point roomPoint)
        {
            int returnDirection = -1;
            List<int> availableDirections = new List<int>();

            for (int i = 0; i < RoomNeighborEnum.ALL_DIRECTIONS.Count; i++)
            {
                int directionMaxShift = getMaxShiftForDirection(roomPoint, i);
                if (directionMaxShift == -1)
                    continue;

                bool directionAvailable = true;
                // + 1 - to look not on corridor, but on key room positions
                for (int s = 1; s <= directionMaxShift; s++)
                {
                    Point keyRoomPoint = getRoomPointByDirectionAndShift(roomPoint, i, s);
                    if (model.Rooms[keyRoomPoint.X, keyRoomPoint.Y] != null)
                    {
                        directionAvailable = false;
                        break;
                    }
                }

                if (directionAvailable)
                    availableDirections.Add(i);
            }

            returnDirection = availableDirections[Random.Range(0, availableDirections.Count)];
            return returnDirection;
        }

        private void updateAvailableKeyRooms()
        {
            List<Point> keyRoomPointsToRemove = new List<Point>();

            for (int a = 0; a < availableKeyRoomPoints.Count; a++)
            {
                Point availableKeyRoomPoint = availableKeyRoomPoints[a];
                List<bool> directions = new List<bool>() {false, false, false, false};

                for (int i = 0; i < RoomNeighborEnum.ALL_DIRECTIONS.Count; i++)
                {
                    int directionMaxShift = getMaxShiftForDirection(availableKeyRoomPoint, i);
                    if (directionMaxShift == -1)
                        continue;

                    directions[i] = true;
                    // + 1 - to look not on corridor, but on key room positions
                    for (int s = 1; s <= directionMaxShift; s++)
                    {
                        Point roomPointWithShift = getRoomPointByDirectionAndShift(availableKeyRoomPoint, i, s);
                        if (model.Rooms[roomPointWithShift.X, roomPointWithShift.Y] != null)
                        {
                            directions[i] = false;
                            break;
                        }
                    }

                    if (directions[i])
                        break;
                }

                bool toRemove = true;
                for (int i = 0; i < directions.Count; i++)
                {
                    if (directions[i])
                    {
                        toRemove = false;
                        break;
                    }
                }

                if (toRemove)
                    keyRoomPointsToRemove.Add(availableKeyRoomPoint);
            }

            while (keyRoomPointsToRemove.Count > 0)
            {
                availableKeyRoomPoints.Remove(keyRoomPointsToRemove[0]);
                keyRoomPointsToRemove.RemoveAt(0);
            }
        }

        private Point getRoomPointByDirectionAndShift(Point roomPos, int direction, int shift)
        {
            Point roomPoint = null;
            switch (direction)
            {
                case RoomNeighborEnum.LEFT:
                    roomPoint = new Point(roomPos.X + (shift * -1), roomPos.Y);
                    break;
                case RoomNeighborEnum.RIGHT:
                    roomPoint = new Point(roomPos.X + shift, roomPos.Y);
                    break;
                case RoomNeighborEnum.UP:
                    roomPoint = new Point(roomPos.X, roomPos.Y + shift);
                    break;
                case RoomNeighborEnum.DOWN:
                    roomPoint = new Point(roomPos.X, roomPos.Y + (shift * -1));
                    break;
            }

            return roomPoint;
        }

        private Point getConnectionForDirection(Point roomPoint, int direction)
        {
            Point connection; // Key Point
            RoomData roomData;
            int directionMaxShift = getMaxShiftForDirection(roomPoint, direction);
            // + 1 - to look not on corridor, but on key room positions
            for (int i = settings.CorridorRoomsSequenceMinNumToMakeConnection + 1; i <= directionMaxShift; i++)
            {
                connection = getRoomPointByDirectionAndShift(roomPoint, direction, i);

                roomData = model.Rooms[connection.X, connection.Y];
                if (roomData != null && roomData.Type == RoomTypeEnum.KEY_POINT)
                {
                    // Only one (per possible pos) connection can be per direction.
                    return connection;
                }
            }
            return null;
        }

        private int getMaxShiftForDirection(Point roomPoint, int direction)
        {
            int maxShift = -1;
            Point roomPointWithShift;
            // +1 - because + key room
            for (int i = settings.CorridorRoomsSequenceMaxNum + 1; i >= settings.CorridorRoomsSequenceMinNum + 1; i--)
            {
                roomPointWithShift = getRoomPointByDirectionAndShift(roomPoint, direction, i);
                if (positionInDungeonBounds(roomPointWithShift))
                {
                    maxShift = i;
                    break;
                }
            }

            return maxShift;
        }

        private bool positionInDungeonBounds(Point point)
        {
            bool result = (point.X >= 0 && point.X < model.Rooms.Width && point.Y >= 0 && point.Y < model.Rooms.Height);
            return result;
        }

        private int getCorridorRoomsSequenceNum(Point connection)
        {
            int difX = Mathf.Abs(connection.X - nextKeyRoomPoint.X);
            int difY = Mathf.Abs(connection.Y - nextKeyRoomPoint.Y);

            int num = 0;
            if (difX > 0)
                num = difX;
            else if (difY > 0)
                num = difY;
            else
                throw new InvalidOperationException("getCorridorRoomsSequenceNum() difX:" + difX + " | difY:" + difY);

            // -1 because... Example: 1-4. Between 1 and 4 should be 2 corridors "2" and "3".
            return num - 1;
        }

        private void initSettingsAndData()
        {
            // TODO: Add handle of passed level of dungeon
            settings = buildingSettingsFactory.CreateSettings();
            availableKeyRoomPoints = new List<Point>();

            model.Rooms = new Flatten2DArray<RoomData>(settings.Width, settings.Height);
            setStartPoint();
        }

        private void setStartPoint()
        {
            int randomX = Random.Range(0, settings.Width);
            int randomY = Random.Range(0, settings.Height);

            nextKeyRoomPoint = new Point(randomX, randomY);
        }

        // Remove "blank" rows/columns
        private void correctRoomsPositionsMatrix()
        {
            // ++++++++++++++++++++++++++ SAVE BORDER POSITIONS ++++++++++++++++++++++++++
            List<RoomData> rooms = model.Rooms.Items.Select(r => r).Where(r => r != null).ToList();

            int minX = rooms.MinBy(r => r.Point.X).Point.X;
            int minY = rooms.MinBy(r => r.Point.Y).Point.Y;

            int maxX = rooms.MaxBy(r => r.Point.X).Point.X;
            int maxY = rooms.MaxBy(r => r.Point.Y).Point.Y;
            // -------------------------- SAVE BORDER POSITIONS --------------------------

            // ++++++++++++++++++++++++++ CREATE CORRECTED MATRIX ++++++++++++++++++++++++++
            // +1 - because 8-3 is = 5, but its "6" items in row. Example: 0010000100
            int width = maxX - minX + 1;
            int height = maxY - minY + 1;

            model.Rooms = ArrayUtils.ResizeArray<RoomData>(model.Rooms, width, height, minX, minY);
            DebugUtil.Log("New matrix size. width: " + width + " | height: " + height);
            // -------------------------- CREATE CORRECTED MATRIX --------------------------
        }

        //--------------------------------------------------------------------------
        //								HANDLERS
        //--------------------------------------------------------------------------
        private void generateDungeonMapHandler()
        {
            initSettingsAndData();

            for (int i = 0; i < settings.KeyRoomsOnMapNum; i++)
            {
                if (roomIDCounter > 0)
                {
                    if (availableKeyRoomPoints.Count == 0)
                        break;

                    // We need to find where key room will be placed, before adding corridor rooms.
                    // Mostly, we need this, because next key room can connect with other key rooms, so, more corridors will be required.
                    findNextKeyRoomPos();

                    // +++++++++++++++++++++++++++++ ADD CORRIDOR ROOMS +++++++++++++++++++++++++++++
                    List<int> directions = nextKeyRoomPointConnections.Keys.ToList();
                    for (int c = 0; c < directions.Count; c++)
                    {
                        // ++++++++++ PROBABILITY CHECK ++++++++++
                        if (RoomNeighborEnum.GetOpositeDirection(nextKeyRoomDirection) == directions[c] &&
                            Random.Range(1, 101) > settings.KeyRoomConnectionProbabilityIfSourceOfDirection)
                        {
                            // If "source" of next key room/direction
                            DebugUtil.Log(
                                "UnityEngine.Random.value > settings.KeyRoomConnectionProbabilityIfSourceOfDirection - CHECK FAILED");
                            continue; // With current config, this should never happen
                        }
                        else if ( /*getRoomNeighbors(nextKeyRoomPos).Count > 2 &&*/
                            RoomNeighborEnum.GetOpositeDirection(nextKeyRoomDirection) != directions[c] &&
                            Random.Range(1, 101) >
                            settings.KeyRoomConnectionProbabilityForNonSourceOfDirection)
                        {
                            // If NOT "source" of next key room/direction
                            //DebugUtil.Log("UnityEngine.Random.value > settings.KeyRoomConnectionProbabilityForNonSourceOfDirection - CHECK NOT PASSED");
                            continue; // This can/should happen sometimes
                        }
                        // ---------- PROBABILITY CHECK ----------

                        Point connection = nextKeyRoomPointConnections[directions[c]];
                        // More like amount or quantity, but let it be "num"
                        int corridorRoomsSequenceNum = getCorridorRoomsSequenceNum(connection);
                        for (int j = 1; j <= corridorRoomsSequenceNum; j++)
                        {
                            Point corridorRoomPoint = getRoomPointByDirectionAndShift(connection,
                                RoomNeighborEnum.GetOpositeDirection(directions[c]), j);
                            addCorridorRoom(corridorRoomPoint);
                        }
                    }
                    // ----------------------------- ADD CORRIDOR ROOMS -----------------------------
                }

                addKeyRoom();
            }

            correctRoomsPositionsMatrix();
            FireMessage(GameMessageEnum.DUNGEON_MAP_LOG);
            FireMessage(GameMessageEnum.DUNGEON_MAP_GENERATED);
        }

        //--------------------------------------------------------------------------
        //							GETTERS/SETTERS (PROPERTIES)
        //--------------------------------------------------------------------------
        public DungeonData model
        {
            get { return GameModel.Instance.Dungeon; }
        }

        public override string Name
        {
            get { return "DungeonMapGenerateController"; }
        }
    }
}