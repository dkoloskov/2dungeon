﻿using enums;
using graph.node;
using model;
using SimpleJSON;
using unity_utils.debug;

namespace game_graph.fsm.states.choose_dungeon.controller.load_map
{
    class DungeonMapLoadController : Node
    {
        public DungeonMapLoadController(string path)
            : base(path)
        {
        }

        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        protected override void setupHandlers()
        {
            base.setupHandlers();
            AddHandler(GameMessageEnum.DUNGEON_MAP_LOAD, mapLoadHandler);
        }

        private void loadMap()
        {
            var n = JSON.Parse(TempJsonMapsData.Map1);
            DebugUtil.Log("map size: " + n["map_size"]["w"].Value + "x" + n["map_size"]["h"].Value);

            FireMessage(GameMessageEnum.DUNGEON_MAP_LOADED);
        }

        //--------------------------------------------------------------------------
        //								HANDLERS
        //--------------------------------------------------------------------------
        private void mapLoadHandler()
        {
            loadMap();
        }

        //--------------------------------------------------------------------------
        //							GETTERS/SETTERS (PROPERTIES)
        //--------------------------------------------------------------------------
        private GameModel model
        {
            get { return GameModel.Instance; }
        }

        public override string Name
        {
            get { return "DungeonMapLoadController"; }
        }
    }
}