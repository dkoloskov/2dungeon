﻿using enums.dungeon.building;
using model.dungeon.building;
using UnityEngine;

namespace game_graph.fsm.states.choose_dungeon.controller.generate_map
{
    internal class DungeonBuildingSettingsFactory
    {
        // Number of corridor rooms, between keyPoints
        private const int DEFAULT_CORRIDOR_ROOMS_SEQUENCE_NUM = 4; // DD default value

        public DungeonBuildingSettingsFactory()
        {
        }

        //--------------------------------------------------------------------------
        //							PUBLIC METHODS
        //--------------------------------------------------------------------------
        public DungeonBuildingSettings CreateSettings(int type = DungeonBuildingSettingsEnum.MINE)
        {
            DungeonBuildingSettings settings = null;

            switch (type)
            {
                case DungeonBuildingSettingsEnum.MINE:
                    settings = createMineSettings();
                    break;
                case DungeonBuildingSettingsEnum.DD:
                    settings = createDDSettings();
                    break;
            }
            return settings;
        }

        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        private DungeonBuildingSettings createMineSettings()
        {
            DungeonBuildingSettings settings = new DungeonBuildingSettings();
            settings.KeyRoomsOnMapMinNum = 8; // 20;
            settings.KeyRoomsOnMapMaxNum = 12; // 30
            settings.KeyRoomsOnMapNum = Random.Range(settings.KeyRoomsOnMapMinNum, settings.KeyRoomsOnMapMaxNum + 1);

            settings.CorridorRoomsSequenceMinNum = DEFAULT_CORRIDOR_ROOMS_SEQUENCE_NUM;
            settings.CorridorRoomsSequenceMaxNum = 6; // Can be more, if key points need to be connected.
            settings.CorridorRoomsSequenceMinNumToMakeConnection = 0;

            settings.KeyRoomConnectionProbabilityIfSourceOfDirection = 100;
            settings.KeyRoomConnectionProbabilityForNonSourceOfDirection = 60;

            // TODO: Choose one of random size patterns
            settings.Width = 50; // 1_st key room + (6 corridor rooms + key room) * 7 = width 8
            settings.Height = 15; // height 3

            return settings;
        }

        // TODO: remove duplicate code.
        private DungeonBuildingSettings createDDSettings()
        {
            DungeonBuildingSettings settings = new DungeonBuildingSettings();
            settings.KeyRoomsOnMapMinNum = 8;
            settings.KeyRoomsOnMapMaxNum = 12;
            settings.KeyRoomsOnMapNum = Random.Range(settings.KeyRoomsOnMapMinNum, settings.KeyRoomsOnMapMaxNum + 1);

            settings.CorridorRoomsSequenceMinNum = DEFAULT_CORRIDOR_ROOMS_SEQUENCE_NUM;
            settings.CorridorRoomsSequenceMinNum = DEFAULT_CORRIDOR_ROOMS_SEQUENCE_NUM;
            settings.CorridorRoomsSequenceMinNumToMakeConnection = 0;

            settings.KeyRoomConnectionProbabilityIfSourceOfDirection = 100;
            settings.KeyRoomConnectionProbabilityForNonSourceOfDirection = 60;

            settings.Width = 16;
            settings.Height = 6;

            return settings;
        }
    }
}