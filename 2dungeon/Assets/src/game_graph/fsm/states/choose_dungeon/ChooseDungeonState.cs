﻿using enums;
using game_graph.fsm.states.choose_dungeon.controller;
using game_graph.fsm.states.choose_dungeon.controller.load_map;
using game_graph.fsm.states.choose_dungeon.controller.generate_map;
using game_graph.fsm.states.state_base;
using graph.enums;
using unity_utils.debug;
using UnityEngine;

namespace game_graph.fsm.states.choose_dungeon
{
    class ChooseDungeonState: GameStateBase
    {
        public ChooseDungeonState(string path, PassTypesEnum passType = PassTypesEnum.ANY) 
            :base(path, passType)
        {
        }

        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        protected override void setupChildren()
        {
            base.setupChildren();
            AddNode(new DungeonMapGenerateController(_path));
            AddNode(new DungeonMapLoadController(_path));

            if (Debug.isDebugBuild)
                AddNode(new DungeonMapLogController(_path));
        }

        override protected void setupHandlers()
        {
            base.setupHandlers();
            AddHandler(GameMessageEnum.DUNGEON_MAP_GENERATED, dungeonMapGeneratedHandler);
            AddHandler(GameMessageEnum.DUNGEON_MAP_LOADED, dungeonMapLoadedHandler);
        }

        protected override void activate()
        {
            base.activate();

            FireMessage(GameMessageEnum.GENERATE_DUNGEON_MAP);
            //FireMessage(GameMessageEnum.DUNGEON_MAP_LOAD);
        }

        //--------------------------------------------------------------------------
        //								HANDLERS
        //--------------------------------------------------------------------------
        private void dungeonMapGeneratedHandler(MessageDirectionsEnum direction)
        {
            if(direction == MessageDirectionsEnum.DOWN)
            {
                DebugUtil.Log(Name + " dungeonMapGeneratedHandler");
                changeState(GameStateEnum.DUNGEON);
            }
        }

        private void dungeonMapLoadedHandler(MessageDirectionsEnum direction)
        {
            if(direction == MessageDirectionsEnum.DOWN)
            {
                DebugUtil.Log(Name + " dungeonMapLoadedHandler");
                changeState(GameStateEnum.DUNGEON);
            }
        }

        //--------------------------------------------------------------------------
        //							GETTERS/SETTERS (PROPERTIES)
        //--------------------------------------------------------------------------
        public override string Name
        {
            get { return GameStateEnum.CHOOSE_DUNGEON; }
        }
    }
}
