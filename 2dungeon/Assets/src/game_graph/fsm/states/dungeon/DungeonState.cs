﻿using System;
using System.Collections.Generic;
using graph.enums;
using enums;
using game_graph.fsm.states.state_base;


namespace game_graph.fsm.states.dungeon
{
    class DungeonState : GameStateBase
    {
        public DungeonState(string path, PassTypesEnum passType = PassTypesEnum.ANY) : base(path, passType)
        {
        }

        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        protected override void activate()
        {
            base.activate();
        }

        //--------------------------------------------------------------------------
        //								HANDLERS
        //--------------------------------------------------------------------------

        //--------------------------------------------------------------------------
        //							GETTERS/SETTERS (PROPERTIES)
        //--------------------------------------------------------------------------
        public override string Name
        {
            get { return GameStateEnum.DUNGEON; }
        }
    }
}
