﻿using graph.node;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using graph.enums;

namespace game_graph.service.touch
{
    abstract class BaseTouchService:Node
    {
        public BaseTouchService(string path) 
        :base(path)
        {
        }

        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        protected abstract string getColiderName();

        protected virtual void sendMessage(string message)
        {
            string coliderName = getColiderName();
            if (coliderName != null)
                FireMessage<string>(message, coliderName);
        }

        //--------------------------------------------------------------------------
        //								HANDLERS
        //--------------------------------------------------------------------------

        //--------------------------------------------------------------------------
        //							GETTERS/SETTERS (PROPERTIES)
        //--------------------------------------------------------------------------
    }
}
