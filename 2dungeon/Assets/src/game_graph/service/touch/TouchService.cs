﻿using enums;
using graph.node;
using System;
using System.Collections.Generic;
using UnityEngine;


namespace game_graph.service.touch
{
    class TouchService:BaseTouchService
    {
        public TouchService(string path) 
        :base(path)
        {
        }

        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        protected override void setupHandlers()
        {
            base.setupHandlers();

            UnityGlobal.AddUpdateHandler(touchHandler);
        }

        protected override string getColiderName()
        {
            string coliderName = null;

            Touch touch = Input.GetTouch(0);
            Vector3 pos = Camera.main.ScreenToWorldPoint(touch.position);
            RaycastHit2D hit = Physics2D.Raycast(pos, Vector2.zero);
            if (hit.collider != null)
                coliderName = hit.collider.name;

            //Debug.Log("touch coliderName: " + coliderName + " | touch.phase: " + touch.phase + " | Time.deltaTime: " + Time.deltaTime);
            return coliderName;
        }

        //--------------------------------------------------------------------------
        //								HANDLERS
        //--------------------------------------------------------------------------
        private void touchHandler()
        {
            if (Input.touchCount == 1)
            {
               // sendMessage(GameMessageEnum.TOUCH);
            }
        }

        //--------------------------------------------------------------------------
        //							GETTERS/SETTERS (PROPERTIES)
        //--------------------------------------------------------------------------
        public override string Name
        {
            get { return "TouchService"; }
        }
    }
}
