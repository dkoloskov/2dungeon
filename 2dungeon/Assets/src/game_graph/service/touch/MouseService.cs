﻿using enums;
using UnityEngine;

namespace game_graph.service.touch
{
    class MouseService : BaseTouchService
    {
        public MouseService(string path)
            : base(path)
        {
        }

        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        protected override void setupHandlers()
        {
            base.setupHandlers();

            UnityGlobal.AddUpdateHandler(mouseDownHandler);
        }

        protected override string getColiderName()
        {
            string coliderName = null;

            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(pos, Vector2.zero);
            
            if (hit.collider != null)
                coliderName = hit.collider.name;
            return coliderName;
        }

        //--------------------------------------------------------------------------
        //								HANDLERS
        //--------------------------------------------------------------------------
        private void mouseDownHandler()
        {
            if (Input.GetMouseButtonDown(0))
            {
                // LEFT mouse btn - CLICK
                //sendMessage(GameMessageEnum.MOUSE_CLICK);
            }
            else if (Input.GetMouseButton(0))
            {
                // LEFT mouse btn - PRESSED
                //sendMessage(GameMessageEnum.MOUSE_DOWN);
            }
        }

        //--------------------------------------------------------------------------
        //							GETTERS/SETTERS (PROPERTIES)
        //--------------------------------------------------------------------------
        public override string Name
        {
            get { return "MouseService"; }
        }
    }
}