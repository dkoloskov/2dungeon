﻿using enums;
using enums.dungeon;
using graph.node;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace game_graph.service
{
    class KeyboardService:Node
    {
        public KeyboardService(string path) 
        :base(path)
        {
        }

        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        protected override void setupHandlers()
        {
            base.setupHandlers();

            UnityGlobal.AddUpdateHandler(keyDownHorizontalHandler);
        }

        //--------------------------------------------------------------------------
        //								HANDLERS
        //--------------------------------------------------------------------------
        // For Move: Left/Right
        private void keyDownHorizontalHandler()
        {
            float horizontalValue = Input.GetAxis("Horizontal");
            if (horizontalValue != 0)
            {
                MoveGroupDirectionEnum direction = (horizontalValue < 0) ? MoveGroupDirectionEnum.LEFT : MoveGroupDirectionEnum.RIGHT;
                // TODO: Change message to KEYBOARD BUTTON PRESSED or semilar
                FireMessage<MoveGroupDirectionEnum>(GameMessageEnum.DUNGEON_MOVE_GROUP, direction);
            }
        }

        //--------------------------------------------------------------------------
        //							GETTERS/SETTERS (PROPERTIES)
        //--------------------------------------------------------------------------
        public override string Name
        {
            get { return "KeyboardService"; }
        }
    }
}
