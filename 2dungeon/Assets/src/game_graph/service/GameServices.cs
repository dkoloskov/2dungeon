﻿using System.Diagnostics;
using graph.node;

namespace game_graph.service
{
    class GameServices : Node
    {
        public GameServices(string path)
            : base(path)
        {
        }

        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        protected override void setupChildren()
        {
            base.setupChildren();

            setupcChildrenPC();
            setupcChildrenMobile();
        }

        [Conditional("UNITY_STANDALONE_WIN")]
        private void setupcChildrenPC()
        {
            AddNode(new KeyboardService(_path));
            // TODO: Think about removing this class
            //AddNode(new MouseService(_path));
        }

        [Conditional("UNITY_ANDROID")]
        private void setupcChildrenMobile()
        {
            // TODO: Think about removing this class
            //AddNode(new TouchService(_path));
        }

        //--------------------------------------------------------------------------
        //							GETTERS/SETTERS (PROPERTIES)
        //--------------------------------------------------------------------------
        public override string Name
        {
            get { return "GameServices"; }
        }
    }
}