﻿using System.Collections.Generic;
using enums;
using enums.dungeon;
using game_graph.view.view_base;
using graph.enums;
using model;
using unity_utils.debug;
using UnityEngine;

namespace game_graph.view.ui.dungeon
{
    class DungeonMapView : BaseStateUIView
    {
        private GameObject corridorRoomIconPrefab;
        private GameObject keyRoomIconPrefab;

        private Vector2 startPos; // Center positon on map

        private List<GameObject> rooms;


        public DungeonMapView(string path, GameObject parentView = null, PassTypesEnum passType = PassTypesEnum.ANY)
            : base(path, parentView, passType)
        {
            init();
        }

        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        private void init()
        {
            loadPrefabs();
            setStartPos();

            rooms = new List<GameObject>();
            addKeyRoom(RoomTypeEnum.KEY_POINT, startPos);
            for (int i = 1; i <= 6; i++)
            {
                addKeyRoom(RoomTypeEnum.CORRIDOR, new Vector2((startPos.x + 105*i), startPos.y));

                addKeyRoom(RoomTypeEnum.CORRIDOR, new Vector2((startPos.x - 105*i), startPos.y));

                addKeyRoom(RoomTypeEnum.CORRIDOR, new Vector2(startPos.x, (startPos.y + 105*i)));

                addKeyRoom(RoomTypeEnum.CORRIDOR, new Vector2(startPos.x, (startPos.y - 105*i)));
            }

            //setViewSize();
        }

        private void addKeyRoom(RoomTypeEnum type, Vector2 pos)
        {
            GameObject prefab = (type == RoomTypeEnum.KEY_POINT) ? keyRoomIconPrefab : corridorRoomIconPrefab;
            GameObject room = GameObject.Instantiate(prefab, Vector3.zero, prefab.transform.rotation);

            addUnityChild(room);
            room.transform.localScale = Vector3.one;
            room.transform.localPosition = pos;

            rooms.Add(room);
        }

        // TODO: Move this to Utils
        private void setViewSize()
        {
            Vector2 min = new Vector2(999, 999);
            Vector2 max = new Vector2(-999, -999);

            for (int i = 0; i < rooms.Count; i++)
            {
                Vector3 roomPos = rooms[i].transform.localPosition;
                // SET MIN/MAX X
                if (roomPos.x < min.x)
                    min.x = roomPos.x;

                if (roomPos.x > max.x)
                    max.x = roomPos.x;

                // SET MIN/MAX Y
                if (roomPos.y < min.y)
                    min.y = roomPos.y;

                if (roomPos.y > max.y)
                    max.y = roomPos.y;
            }


            RectTransform viewRectTransform = _view.GetComponent<RectTransform>();
            Rect roomRect = keyRoomIconPrefab.GetComponent<RectTransform>().rect;
            viewRectTransform.sizeDelta = new Vector2((max.x + roomRect.width  - min.x), (max.y + roomRect.height  - min.y));
            //viewRect.width = max.x + roomRect.width  - min.x;
            //viewRect.height = max.y + roomRect.height  - min.y;
            //DebugUtil.Log("viewRect.width: " + viewRect.width);
            //DebugUtil.Log("viewRect.height: " + viewRect.height);
        }

        private void setStartPos()
        {
            Rect viewRect = _view.GetComponent<RectTransform>().rect;
            Rect roomRect = keyRoomIconPrefab.GetComponent<RectTransform>().rect;

            float x = viewRect.width / 2 - roomRect.width / 2;
            float y = viewRect.height / 2 - roomRect.height / 2;
            startPos = new Vector2(x, y);
        }

        private void loadPrefabs()
        {
            corridorRoomIconPrefab = Resources.Load<GameObject>(Config.UI_MAP_ICONS_PATH + "/map_corridor_room");
            keyRoomIconPrefab = Resources.Load<GameObject>(Config.UI_MAP_ICONS_PATH + "/map_key_room");
        }

        protected override void setupGraphics()
        {
            _view = GameObject.Find("MapContainer");
        }

        protected override void setupHandlers()
        {
            base.setupHandlers();
            listenStateActivated(GameStateEnum.DUNGEON);
        }

        //--------------------------------------------------------------------------
        //								HANDLERS
        //--------------------------------------------------------------------------
        /*protected virtual void buttonDownHandler(string buttonName)
        {
            
        }*/

        //--------------------------------------------------------------------------
        //							GETTERS/SETTERS (PROPERTIES)
        //--------------------------------------------------------------------------
        public override string Name
        {
            get { return "DungeonMapView"; }
        }
    }
}