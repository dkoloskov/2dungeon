﻿using enums;
using enums.dungeon;
using game_graph.view.view_base;
using graph.enums;
using unity_utils;
using UnityEngine;
using UnityEngine.EventSystems;

namespace game_graph.view.ui.dungeon
{
    public class DungeonUIView : BaseStateUIView
    {
        //protected Dictionary<string, GameObject> buttons;

        private GameObject moveLeftBtn;
        private const string MOVE_LEFT_BTN_NAME = "MoveLeftBtn";
        private bool moveLeft = false;

        private GameObject moveRightBtn;
        private const string MOVE_RIGHT_BTN_NAME = "MoveRightBtn";
        private bool moveRight = false;


        public DungeonUIView(string path, GameObject parentView = null, PassTypesEnum passType = PassTypesEnum.ANY)
            : base(path, parentView, passType)
        {
        }

        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        protected override void setupGraphics()
        {
            //buttons = new Dictionary<string, GameObject>();

            moveLeftBtn = GameObject.Find(MOVE_LEFT_BTN_NAME);
            moveRightBtn = GameObject.Find(MOVE_RIGHT_BTN_NAME);
        }

        protected override void activate()
        {
            Debug.Log(Name + " view activate");
            //_view.SetActive(true);
        }

        // TODO: Clean code
        /*private void addButton(string btnName, Vector2 position)
        {
            GameObject btn = buttons[btnName] =
                UnityUtils.DrawRect(btnName, new Rect(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT), Color.white, true);
            UnityUtils.SetAlpha(btn, 0.2f);
            btn.transform.position = position;
            addUnityChild(btn);
        }*/

        private void addMoveButtonHandlers(GameObject btn)
        {
            UnityUtils.AddListenerPointerDown(btn, onMoveBtnDown);
            UnityUtils.AddListenerPointerEnter(btn, onMoveBtnDown);

            UnityUtils.AddListenerPointerUp(btn, OnMoveBtnUp);
            UnityUtils.AddListenerPointerExit(btn, OnMoveBtnUp);
        }

        protected override void setupHandlers()
        {
            base.setupHandlers();
            listenStateActivated(GameStateEnum.DUNGEON);

            addMoveButtonHandlers(moveLeftBtn);
            addMoveButtonHandlers(moveRightBtn);

            UnityGlobal.AddUpdateHandler(pointerDownUpdateHandler);
        }

        //--------------------------------------------------------------------------
        //								HANDLERS
        //--------------------------------------------------------------------------
        protected void onMoveBtnDown(PointerEventData eventData)
        {
            if (!UnityUtils.PointerDown)
                return;

            string btnName = eventData.pointerCurrentRaycast.gameObject.name;
            switch (btnName)
            {
                case MOVE_LEFT_BTN_NAME:
                    moveLeft = true;
                    break;
                case MOVE_RIGHT_BTN_NAME:
                    moveRight = true;
                    break;
            }
        }

        private void OnMoveBtnUp(PointerEventData eventData)
        {
            moveLeft = false;
            moveRight = false;
        }

        private void pointerDownUpdateHandler()
        {
            if (moveLeft)
                FireMessage<MoveGroupDirectionEnum>(GameMessageEnum.DUNGEON_MOVE_GROUP,
                    MoveGroupDirectionEnum.LEFT);

            if (moveRight)
                FireMessage<MoveGroupDirectionEnum>(GameMessageEnum.DUNGEON_MOVE_GROUP,
                    MoveGroupDirectionEnum.RIGHT);
        }

        //--------------------------------------------------------------------------
        //							GETTERS/SETTERS (PROPERTIES)
        //--------------------------------------------------------------------------
        public override string Name
        {
            get { return "DungeonUIView"; }
        }
    }
}