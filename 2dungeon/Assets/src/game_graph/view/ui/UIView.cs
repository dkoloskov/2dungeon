﻿using game_graph.view.ui.dungeon;
using game_graph.view.view_base;
using graph.enums;
using UnityEngine;

namespace game_graph.view.ui
{
    class UIView : BaseNodeUIView
    {
        public UIView(string path, GameObject parentView = null, PassTypesEnum passType = PassTypesEnum.ANY)
            : base(path, parentView, passType)
        {
        }

        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        protected override void setupChildren()
        {
            base.setupChildren();

            AddNode(new DungeonMapView(_path, _view));
            AddNode(new DungeonUIView(_path, _view));
        }

        // TODO: Clean code
        /*[Conditional("UNITY_STANDALONE_WIN")]
        private void setupcChildrenPC()
        {
            AddNode(new DungeonUIViewPC(_path, _view));
        }

        [Conditional("UNITY_ANDROID")]
        private void setupcChildrenMobile()
        {
            AddNode(new DungeonUIViewMobile(_path, _view));
        }*/

        //--------------------------------------------------------------------------
        //							GETTERS/SETTERS (PROPERTIES)
        //--------------------------------------------------------------------------
        public override string Name
        {
            get { return "UIView"; }
        }
    }
}