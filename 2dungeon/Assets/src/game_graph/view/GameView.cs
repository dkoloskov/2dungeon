﻿using System;
using System.Collections.Generic;
using graph.enums;
using UnityEngine;
using game_graph.view.view_base;
using game_graph.view.dungeon;
using game_graph.view.ui;


namespace game_graph.view
{
    class GameView : BaseNodeView
    {
        public GameView(string path, GameObject parentView = null, PassTypesEnum passType = PassTypesEnum.ANY) 
            :base(path, parentView, passType)
        {
        }

        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        protected override void setupChildren()
        {
            base.setupChildren();

            AddNode(new UIView(_path, _view));
            AddNode(new DungeonView(_path, _view));
        }

        //--------------------------------------------------------------------------
        //							GETTERS/SETTERS (PROPERTIES)
        //--------------------------------------------------------------------------
        public override string Name
        {
            get { return "GameView"; }
        }
    }
}
