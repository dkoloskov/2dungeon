﻿using System;
using System.Collections.Generic;
using graph.enums;
using UnityEngine;
using game_graph.view.view_base;
using game_graph.view.dungeon.background;
using enums;


namespace game_graph.view.dungeon
{
    class DungeonView : BaseStateView
    {
        public DungeonView(string path, GameObject parentView, PassTypesEnum passType = PassTypesEnum.ANY) : 
            base(path, parentView, passType)
        {
        }

        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        protected override void setupChildren()
        {
            base.setupChildren();
            AddNode(new DungeonBackgroundView(_path, _view));
        }

        protected override void setupHandlers()
        {
            base.setupHandlers();
            listenStateActivated(GameStateEnum.DUNGEON);
        }

        protected override void activate()
        {
            base.activate();
        }

        //--------------------------------------------------------------------------
        //							GETTERS/SETTERS (PROPERTIES)
        //--------------------------------------------------------------------------
        public override string Name
        {
            get { return "DungeonView"; }
        }
    }
}
