﻿using System;
using System.Collections.Generic;
using graph.enums;
using UnityEngine;
using game_graph.view.view_base;
using model;
using enums;
using unity_utils;
using enums.dungeon;


namespace game_graph.view.dungeon.background
{
    class DungeonBackgroundView : BaseNodeView
    {
        private List<GameObject> wallPrefabs;
        private float maxLeftX;


        public DungeonBackgroundView(string path, GameObject parentView, PassTypesEnum passType = PassTypesEnum.ANY)
            :base(path, parentView, passType)
        {
        }

        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        protected override void setupGraphics()
        {
            base.setupGraphics();
            _view.GetComponent<Renderer>().sortingLayerName = SortingLayersEnum.BACKGROUND;
            this.loadPrefabs();

            GameObject wallPrefab;
            GameObject wall;
            Vector2 position;
            float shiftX = 0;
            int randomWallNum;
            //for (int i = 0; i < wallPrefabs.Count; i++)
            for (int i = 0; i < 10; i++)
            {
                //wallPrefab = wallPrefabs[i];
                randomWallNum = UnityEngine.Random.Range(0, Config.BACKGROUND_WALLS_NUM);
                wallPrefab = wallPrefabs[randomWallNum];
                position = new Vector2(Config.DUNGEON_BOTTOM_LEFT.x+shiftX, Config.DUNGEON_BOTTOM_LEFT.y);

                wall = GameObject.Instantiate(wallPrefab, position, wallPrefab.transform.rotation);
                addUnityChild(wall);

                shiftX += wall.GetComponent<Renderer>().bounds.size.x;

                /*Debug.Log("wall_" + i + " position: " + wall.transform.position);
                Debug.Log("wall_" + i + " bounds: " + wall.GetComponent<Renderer>().bounds);*/

                //wall.GetComponent<Renderer>().enabled = false;
            }

            maxLeftX = -(shiftX - Screen.currentResolution.width);
        }


        protected override void setupHandlers()
        {
            base.setupHandlers();
            AddHandler<MoveGroupDirectionEnum>(GameMessageEnum.DUNGEON_MOVE_GROUP, moveGroupHandler);
        }

        private void loadPrefabs()
        {
            wallPrefabs = new List<GameObject>();
            GameObject prefab;
            for (int i = 0; i < Config.BACKGROUND_WALLS_NUM; i++)
            {
                prefab = Resources.Load<GameObject>(Config.BACKGROUND_WALLS_PATH + "/wall_" + i);
                wallPrefabs.Add(prefab);
            }
        }

        //--------------------------------------------------------------------------
        //								HANDLERS
        //--------------------------------------------------------------------------
        private void moveGroupHandler(MoveGroupDirectionEnum direction)
        {
            // Time.deltaTime here for - to make speed per second, not per frame rate.
            float destinationX = (int)direction * -1 * Config.MOVE_GROUP_SPEED * Time.deltaTime + _view.transform.position.x;
            // Move background RIGHT
            if (direction == MoveGroupDirectionEnum.LEFT && destinationX > 0)
            {
                destinationX = 0;
            }
            // Move background LEFT
            if (direction == MoveGroupDirectionEnum.RIGHT && destinationX < maxLeftX)
            {
                destinationX = maxLeftX;
            }

            Vector2 destinationPoint = new Vector2(destinationX, _view.transform.position.y);
            _view.transform.position = destinationPoint;
        }

        //--------------------------------------------------------------------------
        //							GETTERS/SETTERS (PROPERTIES)
        //--------------------------------------------------------------------------
        public override string Name
        {
            get { return "DungeonBackgroundView";  }
        }
    }
}
