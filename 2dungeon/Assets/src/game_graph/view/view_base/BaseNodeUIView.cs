﻿using graph.enums;
using UnityEngine;

namespace game_graph.view.view_base
{
    public abstract class BaseNodeUIView : BaseNodeView
    {
        public BaseNodeUIView(string path, GameObject parentView = null,
            PassTypesEnum passType = PassTypesEnum.ANY) : base(path, parentView, passType)
        {
        }

        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        protected override void setupGraphics()
        {
        }

        protected override void addUnityChild(GameObject parent, GameObject child)
        {
            child.transform.SetParent(parent.transform);
            //child.GetComponent<Renderer>().so = parent.GetComponent<Renderer>().sortingLayerName;
        }
    }
}