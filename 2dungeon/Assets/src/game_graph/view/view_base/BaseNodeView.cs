﻿using graph.enums;
using graph.node;
using unity_utils;
using UnityEngine;

namespace game_graph.view.view_base
{
    public abstract class BaseNodeView : Node
    {
        protected GameObject _parentView;
        protected GameObject _view;

        public BaseNodeView(string path, GameObject parentView = null, PassTypesEnum passType = PassTypesEnum.ANY)
            : base(path, passType, false)
        {
            _parentView = parentView;
            setupNode();
        }

        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        protected override void setupChildren()
        {
            setupGraphics();
            base.setupChildren();
        }

        protected virtual void setupGraphics()
        {
            //_view = new GameObject(Name);
            _view = UnityUtils.DrawRect(Name, new Rect(0, 0, 1, 1), Color.red);
            UnityUtils.SetAlpha(_view, 0);

            if (_parentView != null)
            {
                addUnityChild(_parentView, _view);
            }
        }

        protected virtual void addUnityChild(GameObject child)
        {
            addUnityChild(_view, child);
        }

        protected virtual void addUnityChild(GameObject parent, GameObject child)
        {
            child.transform.SetParent(parent.transform);
            child.GetComponent<Renderer>().sortingLayerName = parent.GetComponent<Renderer>().sortingLayerName;
        }

        //--------------------------------------------------------------------------
        //							   DESTROY
        //--------------------------------------------------------------------------
        public override void Dispose()
        {
            base.Dispose();

            GameObject.Destroy(_view);
            _view = null;
            _parentView = null;
        }
    }
}