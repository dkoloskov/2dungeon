﻿using enums;
using graph.enums;
using model;
using UnityEngine;

namespace game_graph.view.view_base
{
    public abstract class BaseStateUIView : BaseNodeUIView
    {
        // TODO: Think how to move logic of BaseStateView and BaseStateUIView to one class
        public BaseStateUIView(string path, GameObject parentView, PassTypesEnum passType = PassTypesEnum.ANY) :
            base(path, parentView, passType)
        {
        }

        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        protected override void setupGraphics()
        {
            base.setupGraphics();
            _view.SetActive(false);
        }

        protected virtual void listenStateActivated(string state)
        {
            AddHandler(GameMessageEnum.STATE_ACTIVATED + Config.STATE_MESSAGE_SEPARATOR + state, activatedHandler);
        }

        protected virtual void activate()
        {
            Debug.Log(Name + " view activate");
            _view.SetActive(true);
        }

        //--------------------------------------------------------------------------
        //								HANDLERS
        //--------------------------------------------------------------------------
        protected void activatedHandler()
        {
            activate();
        }
    }
}