﻿using graph.node;
using System;
using System.Collections.Generic;
using graph.enums;
using model;
using enums;
using game_graph.fsm;
using game_graph.view;
using UnityEngine;
using game_graph.service;


class GameRoot : Node, INode
{
    public GameRoot() 
        :base("", PassTypesEnum.NOWHERE)
    {
    }

    //--------------------------------------------------------------------------
    //					  PRIVATE\PROTECTED METHODS
    //--------------------------------------------------------------------------
    protected override void setupChildren()
    {
        AddNode(new GameFSM(_path));
        AddNode(new GameView(_path));
        AddNode(new GameServices(_path));
    }

    override protected void setupHandlers()
    {
        base.setupHandlers();
        AddHandler(GameMessageEnum.INITIALIZE, gameInitializeHandler);
    }

    //--------------------------------------------------------------------------
    //								HANDLERS
    //--------------------------------------------------------------------------
    // Public only for Graph
    public override void OnBlock<T>(MessageDirectionsEnum direction, string type, T data = default(T))
    {
        FireMessage(type, data, MessageDirectionsEnum.DOWN);
    }

    private void gameInitializeHandler()
    {
        GameModel.Instance.State = GameStateEnum.DEFAULT_STATE;
        FireMessage(GameMessageEnum.CHANGE_STATE, null, MessageDirectionsEnum.DOWN);
    }

    //--------------------------------------------------------------------------
    //							GETTERS/SETTERS (PROPERTIES)
    //--------------------------------------------------------------------------
    public override string Name
    {
        get { return GameModel.ROOT_NAME; }
    }
}