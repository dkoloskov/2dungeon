﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace enums.dungeon
{
    public enum MoveGroupDirectionEnum:int
    {
        LEFT = -1,
        RIGHT = 1
    };
}
