﻿using System;
using System.Collections.Generic;


namespace enums.dungeon
{
    class RoomNeighborEnum
    {
        public const int NO_NEIGHBOR = -1;

        public const int LEFT = 0;
        public const int UP = 1;
        public const int RIGHT = 2;
        public const int DOWN = 3;

        public static readonly List<int> ALL_DIRECTIONS = new List<int> { LEFT, UP, RIGHT, DOWN };

        //--------------------------------------------------------------------------
        //							PUBLIC METHODS
        //--------------------------------------------------------------------------
        public static List<int> GetRelatedDirections(int direction)
        {
            List<int> relatedDirections = new List<int>(ALL_DIRECTIONS);
            int opositeDirection = GetOpositeDirection(direction);
            relatedDirections.Remove(opositeDirection);

            return relatedDirections;
        }

        public static int GetOpositeDirection(int direction)
        {
            int opositeDirection = -1;

            switch(direction)
            {
                case LEFT:
                    opositeDirection = RIGHT;
                    break;
                case RIGHT:
                    opositeDirection = LEFT;
                    break;
                case UP:
                    opositeDirection = DOWN;
                    break;
                case DOWN:
                    opositeDirection = UP;
                    break;
            }

            if(opositeDirection == -1)
                throw new InvalidOperationException("opositeDirection == -1");

            return opositeDirection;
        }
    }
}
