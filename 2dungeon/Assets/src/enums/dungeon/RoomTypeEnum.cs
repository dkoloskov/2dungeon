﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace enums.dungeon
{
    public enum RoomTypeEnum : int
    {
        NO_ROOM = 0,
        KEY_POINT = 1, // "KEY POINT", NOT ROOM WITH KEY!
        CORRIDOR = 2
    };
}
