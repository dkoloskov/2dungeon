﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace enums
{
    class GameMessageEnum
    {
        // BASE / COMMON
        public const string INITIALIZE = "GAME_INITIALIZE"; // Message from outside
        public const string CHANGE_STATE = "GAME_CHANGE_STATE";

        public const string STATE_ACTIVATED = "GAME_STATE_ACTIVATED";

        // CHOOSE DUNGEON STATE
        public const string GENERATE_DUNGEON_MAP = "GAME_GENERATE_DUNGEON_MAP";
        public const string DUNGEON_MAP_GENERATED = "DUNGEON_MAP_GENERATED";

        public const string DUNGEON_MAP_LOG = "GAME_DUNGEON_MAP_LOG";

        public const string DUNGEON_MAP_LOAD = "GAME_DUNGEON_MAP_LOAD";
        public const string DUNGEON_MAP_LOADED = "GAME_DUNGEON_MAP_LOADED";

        // DUNGEON STATE
        public const string DUNGEON_MOVE_GROUP = "GAME_DUNGEON_MOVE_GROUP";
    }
}
