﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace enums
{
    class GameStateEnum
    {
        public const string CHOOSE_DUNGEON = "GAME_CHOOSE_DUNGEON";
        public const string DUNGEON = "GAME_DUNGEON";

        public const string DEFAULT_STATE = CHOOSE_DUNGEON;
    }
}
