﻿#if UNITY_EDITOR

using UnityEngine;
using System.Collections.Generic;
using System.Threading;

#pragma warning disable 0414, 0162

/// <summary>
/// Overrides Unity's Debug class because it's in the root namespace.
/// </summary>
public static class AsyncDebug
{

    public static bool developerConsoleVisible { get { return UnityEngine.Debug.developerConsoleVisible; } set { UnityEngine.Debug.developerConsoleVisible = value; } }
    public static bool isDebugBuild { get { return UnityEngine.Debug.isDebugBuild; } }

    public static void Break() { UnityEngine.Debug.Break(); }
    public static void ClearDeveloperConsole() { UnityEngine.Debug.ClearDeveloperConsole(); }
    public static void DebugBreak() { UnityEngine.Debug.DebugBreak(); }
    public static void DrawLine(Vector3 start, Vector3 end) { UnityEngine.Debug.DrawLine(start, end); }
    public static void DrawLine(Vector3 start, Vector3 end, Color color) { UnityEngine.Debug.DrawLine(start, end, color); }
    public static void DrawLine(Vector3 start, Vector3 end, Color color, float duration) { UnityEngine.Debug.DrawLine(start, end, color, duration); }
    public static void DrawLine(Vector3 start, Vector3 end, Color color, float duration, bool depthTest) { UnityEngine.Debug.DrawLine(start, end, color, duration, depthTest); }
    public static void DrawRay(Vector3 start, Vector3 dir) { UnityEngine.Debug.DrawRay(start, dir); }
    public static void DrawRay(Vector3 start, Vector3 dir, Color color) { UnityEngine.Debug.DrawRay(start, dir, color); }
    public static void DrawRay(Vector3 start, Vector3 dir, Color color, float duration) { UnityEngine.Debug.DrawRay(start, dir, color, duration); }
    public static void DrawRay(Vector3 start, Vector3 dir, Color color, float duration, bool depthTest) { UnityEngine.Debug.DrawRay(start, dir, color, duration, depthTest); }

    public static bool sleep = true; // set to false if timing is important, uses more CPU
    private static Thread thread;
    private static bool threadShouldRun = true;
    private static Queue<Entry> queue;
    private static object lockObject;

    static AsyncDebug()
    {
        lockObject = new object();
        queue = new Queue<Entry>();
        UnityEditor.EditorApplication.update += EditorUpdate;
        thread = new Thread(Update);
        thread.Start();
    }

    private static void EditorUpdate()
    {
        if (!Application.isPlaying) threadShouldRun = false; // kill thread if Play is stopped
    }

    private static void Update()
    {
        while (threadShouldRun)
        {
            lock (lockObject)
            {
                while (queue.Count > 0)
                {
                    Entry entry = queue.Dequeue();
                    switch (entry.mode)
                    {
                        case Mode.Normal:
                            if (entry.context != null) UnityEngine.Debug.Log(entry.obj, entry.context);
                            else UnityEngine.Debug.Log(entry.obj);
                            //System.Console.WriteLine(entry.obj);
                            break;
                        case Mode.Warning:
                            if (entry.context != null) UnityEngine.Debug.LogWarning(entry.obj, entry.context);
                            else UnityEngine.Debug.LogWarning(entry.obj);
                            break;
                        case Mode.Exception:
                            if (entry.context != null) UnityEngine.Debug.LogException(entry.obj as System.Exception, entry.context);
                            else UnityEngine.Debug.LogException(entry.obj as System.Exception);
                            break;
                        case Mode.Error:
                            if (entry.context != null) UnityEngine.Debug.LogError(entry.obj, entry.context);
                            else UnityEngine.Debug.LogError(entry.obj);
                            break;
                    }
                }
            }
            if (sleep) Thread.Sleep(100); // sleep a little to avoid hammering the CPU
        }
    }

    public static void Log(object obj)
    {
        lock (lockObject)
        {
            queue.Enqueue(new Entry()
            {
                obj = obj,
                mode = Mode.Normal
            });
        }
    }
    public static void Log(object obj, Object context)
    {
        lock (lockObject)
        {
            queue.Enqueue(new Entry()
            {
                obj = obj,
                mode = Mode.Normal,
                context = context
            });
        }
    }

    public static void LogWarning(object obj)
    {
        lock (lockObject)
        {
            queue.Enqueue(new Entry()
            {
                obj = obj,
                mode = Mode.Warning
            });
        }
    }
    public static void LogWarning(object obj, Object context)
    {
        lock (lockObject)
        {
            queue.Enqueue(new Entry()
            {
                obj = obj,
                mode = Mode.Warning,
                context = context
            });
        }
    }

    public static void LogError(object obj)
    {
        lock (lockObject)
        {
            queue.Enqueue(new Entry()
            {
                obj = obj,
                mode = Mode.Error
            });
        }
    }
    public static void LogError(object obj, Object context)
    {
        lock (lockObject)
        {
            queue.Enqueue(new Entry()
            {
                obj = obj,
                mode = Mode.Error,
                context = context
            });
        }
    }

    public static void LogException(System.Exception exception)
    {
        lock (lockObject)
        {
            queue.Enqueue(new Entry()
            {
                obj = exception,
                mode = Mode.Exception
            });
        }
    }
    public static void LogException(System.Exception exception, Object context)
    {
        lock (lockObject)
        {
            queue.Enqueue(new Entry()
            {
                obj = exception,
                mode = Mode.Exception,
                context = context
            });
        }
    }

    public static void LogErrorFormat(string format, params object[] args) { UnityEngine.Debug.LogErrorFormat(format, args); }
    public static void LogErrorFormat(Object context, string format, params object[] args) { UnityEngine.Debug.LogErrorFormat(context, format, args); }
    public static void LogFormat(string format, params object[] args) { UnityEngine.Debug.LogFormat(format, args); }
    public static void LogFormat(Object context, string format, params object[] args) { UnityEngine.Debug.LogFormat(context, format, args); }
    public static void LogWarningFormat(string format, params object[] args) { UnityEngine.Debug.LogWarningFormat(format, args); }
    public static void LogWarningFormat(Object context, string format, params object[] args) { UnityEngine.Debug.LogWarningFormat(context, format, args); }


    private struct Entry
    {
        public object obj;
        public Mode mode;
        public Object context;
    }

    private enum Mode
    {
        Normal,
        Warning,
        Error,
        Exception
    }
}
#endif