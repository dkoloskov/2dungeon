﻿using common_lib.graph.fsm;
using model.dungeon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace model
{
    public class GameModel: IStateModel
    {
        public static readonly string ROOT_NAME = "GameRoot";
        private static GameModel _instance;

        private string _state;

        public DungeonData Dungeon;


        static GameModel()
        {
            _instance = new GameModel();
        }

        private GameModel()
        {
            Dungeon = new DungeonData();
        }

        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------

        //--------------------------------------------------------------------------
        //							GETTERS/SETTERS (PROPERTIES)
        //--------------------------------------------------------------------------
        public static GameModel Instance
        {
            get { return _instance; }
        }

        public string State
        {
            get { return _state; }
            set { _state = value; }
        }
    }
}
