﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace model
{
    class Config
    {
        // =============================== RESOLUTION ===============================
        public const int GAME_WIDTH = 1920;
        public const int GAME_HEIGHT = 1080;
        // This ratio should be adapted for other resolutions.
        public const float SCREEN_RATIO = (float)GAME_WIDTH / GAME_HEIGHT;

        public static int BACKGROUND_HEIGHT = 720;

        // UI
        public static int UI_HEIGHT = GAME_HEIGHT - BACKGROUND_HEIGHT;


        // =============================== POSITIONS ===============================
        //public static Vector2 BOTTOM_LEFT = Camera.main.ScreenToWorldPoint(new Vector2(0, (Screen.currentResolution.height - SCREEN_HEIGHT)));
        // This will work, only whyle orthographicSize == SCREEN_HEIGHT/2
        //public static Vector2 BOTTOM_LEFT = new Vector2(-Camera.main.orthographicSize * Camera.main.aspect, (Screen.currentResolution.height - SCREEN_HEIGHT - Camera.main.orthographicSize));
        public static Vector2 BOTTOM_LEFT = new Vector2(-GAME_WIDTH/2, -GAME_HEIGHT/2);
        public static Vector2 DUNGEON_BOTTOM_LEFT = new Vector2(BOTTOM_LEFT.x, BOTTOM_LEFT.y + UI_HEIGHT);


        // =============================== GAMEPLAY ===============================
        public static float MOVE_GROUP_SPEED = 400;

        //
        public static string STATE_MESSAGE_SEPARATOR = "-";

        // =============================== PREFABS ===============================
        // Background
        public static int BACKGROUND_WALLS_NUM = 7;
        public static string BACKGROUND_WALLS_PATH = "background/crypt/corridor";

        public static string UI_MAP_ICONS_PATH = "ui/map_icons";
    }
}