﻿using common_lib.array;

namespace model.dungeon
{
    public class DungeonData
    {
        //public List<List<int>> RoomsAdjacencyTable; // All connected neighbors rooms, for each room (max - 4)
        public Flatten2DArray<RoomData> Rooms; // Matrix of RoomData

        public DungeonData()
        {
            init();
        }

        //--------------------------------------------------------------------------
        //							PUBLIC METHODS
        //--------------------------------------------------------------------------
        public void init()
        {
        }

        //--------------------------------------------------------------------------
        //							GETTERS/SETTERS (PROPERTIES)
        //--------------------------------------------------------------------------
    }
}