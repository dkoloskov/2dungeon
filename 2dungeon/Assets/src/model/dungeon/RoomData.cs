﻿using common_lib.geom;
using enums.dungeon;

namespace model.dungeon
{
    public class RoomData
    {
        public int ID;
        public RoomTypeEnum Type;
        public Point Point;

        // TODO: добавить параметр разведана/исследована.

        public RoomData(RoomTypeEnum type = RoomTypeEnum.NO_ROOM, int id = -1, Point point = null)
        {
            Type = type;
            ID = id;
            Point = point;
        }
    }
}