﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace model.dungeon.building
{
    class DungeonBuildingSettings
    {
        // Size in rooms num
        public int Width;
        public int Height;

        public int KeyRoomsOnMapMinNum;
        public int KeyRoomsOnMapMaxNum;
        public int KeyRoomsOnMapNum; // random of min and max values

        // Number of corridor rooms, between keyPoints.
        public int CorridorRoomsSequenceMinNum;
        public int CorridorRoomsSequenceMaxNum;

        public int CorridorRoomsSequenceMinNumToMakeConnection;

        // Probability from 0% to 100%
        public int KeyRoomConnectionProbabilityIfSourceOfDirection; // If "source" of next key room/direction
        public int KeyRoomConnectionProbabilityForNonSourceOfDirection;


        public DungeonBuildingSettings()
        {

        }
    }
}
